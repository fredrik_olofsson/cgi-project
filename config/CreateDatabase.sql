BEGIN;

CREATE DATABASE IF NOT EXISTS `demo_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `demo_db`;

DROP TABLE IF EXISTS `seat`;

CREATE TABLE `seat` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `seat_number` int(11) NOT NULL,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `UNIQUE_SEAT_NUMBER` (`seat_number`),
  KEY `INDEX_SEAT` (`seat_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

DROP PROCEDURE IF EXISTS `init_seat`;

DELIMITER $$  
CREATE PROCEDURE init_seat()

BEGIN
	DECLARE seat_number INT DEFAULT 1 ;
	INIT_LOOP: LOOP         
		INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, seat_number);
		SET seat_number = seat_number + 1;
		IF seat_number = 101 THEN
			LEAVE INIT_LOOP;
		END IF;
	END LOOP INIT_LOOP;
END $$
DELIMITER ;


CALL `init_seat`;
DROP PROCEDURE `init_seat`;
COMMIT;