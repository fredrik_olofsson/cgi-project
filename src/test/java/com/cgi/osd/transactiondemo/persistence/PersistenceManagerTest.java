package com.cgi.osd.transactiondemo.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for testing the persistence manager.
 *
 */
@RunWith(Arquillian.class)
public class PersistenceManagerTest {

    @Deployment
    public static Archive<WebArchive> createTestArchive() {
	final WebArchive archive = ShrinkWrap.create(WebArchive.class, "persistence-manager-test.war");
	archive.addPackages(true, "com.cgi.osd.transactiondemo.logic");
	archive.addPackages(true, "com.cgi.osd.transactiondemo.persistence");
	archive.addPackages(true, "com.cgi.osd.transactiondemo.util");
	archive.deleteClasses(PersistenceObjectFactoryTest.class);
	archive.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
	archive.addAsWebInfResource("test-ds.xml");
	archive.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	return archive;
    }

    private final int NUMBER_OF_SEATS = 100;

    @Inject
    private PersistenceManager persistenceManager;

    @PersistenceContext(unitName = "primary")
    private EntityManager em;

    @Inject
    UserTransaction utx;

    @Before
    public void initDataBase() {
	try {
	    this.utx.begin();
	    this.em.joinTransaction();
	    emptySeatTable();
	    addSeatsToSeatTable();
	    this.utx.commit();
	} catch (final NotSupportedException e) {
	    fail("NotSupportedException: " + e.getMessage());
	} catch (final SystemException e) {
	    fail("SystemException: " + e.getMessage());
	} catch (final SecurityException e) {
	    fail("SecurityException: " + e.getMessage());
	} catch (final IllegalStateException e) {
	    fail("IllegalStateException: " + e.getMessage());
	} catch (final RollbackException e) {
	    fail("RollbackException: " + e.getMessage());
	} catch (final HeuristicMixedException e) {
	    fail("HeuristicMixedException: " + e.getMessage());
	} catch (final HeuristicRollbackException e) {
	    fail("HeuristicRollbackException: " + e.getMessage());
	}
    }

    @Test
    public void testGetAllSeats() throws PersistenceOperationException {
	final List<SeatDO> seats = this.persistenceManager.getAllSeats();
	for (int i = 0; i < this.NUMBER_OF_SEATS; i++) {
	    final SeatDO seat = seats.get(i);
	    assertEquals(i, seat.getSeatNumber());
	}
    }

    @Test
    public void testInjects() {
	assertNotNull(this.em);
	assertNotNull(this.persistenceManager);
	assertNotNull(this.utx);
    }

    private void addSeatsToSeatTable() {
	final String sqlInsertString = "INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, :seatNumber)";
	final Query query = this.em.createNativeQuery(sqlInsertString);
	for (int i = 0; i < this.NUMBER_OF_SEATS; i++) {
	    query.setParameter("seatNumber", i);
	    query.executeUpdate();
	}
    }

    private void emptySeatTable() {
	final String deleteString = "DELETE FROM `seat`";
	this.em.createNativeQuery(deleteString).executeUpdate();
    }

}
