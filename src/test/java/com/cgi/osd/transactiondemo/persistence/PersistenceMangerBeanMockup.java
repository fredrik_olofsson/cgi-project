package com.cgi.osd.transactiondemo.persistence;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for constituting a test implementation of the PersistenceManager interface.
 *
 *
 */
@Alternative
@ApplicationScoped
public class PersistenceMangerBeanMockup implements PersistenceManager {

    @Inject
    private PersistenceObjectFactory objectFactory;

    @Override
    public List<SeatDO> getAllSeats() throws PersistenceOperationException {
	final Collection<Seat> seats = new LinkedList<>();
	for (int i = 0; i < 100; i++) {
	    seats.add(new Seat(i));
	}
	final List<SeatDO> seatDOs = this.objectFactory.createSeatDOList(seats);
	return seatDOs;
    }

}
