package com.cgi.osd.transactiondemo.logic.domainobject;

/**
 * This class is responsible for implementing the domain object Seat.
 *
 */
public class SeatDO {

    private final int seatNumber;

    public SeatDO(int seatNumber) {
	this.seatNumber = seatNumber;
    }

    public int getSeatNumber() {
	return this.seatNumber;
    }

}
