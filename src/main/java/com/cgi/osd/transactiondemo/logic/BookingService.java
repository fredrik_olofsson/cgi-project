package com.cgi.osd.transactiondemo.logic;

import java.util.List;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This interface defines the method required for handling bookings.
 */
public interface BookingService {

    /**
     * This method returns a list of all seats.
     *
     * @return a list of all seats.
     * @throws PersistenceOperationException
     */
    public List<SeatDO> getAllSeats() throws PersistenceOperationException;

}
