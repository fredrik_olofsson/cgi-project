package com.cgi.osd.transactiondemo.persistence;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

/**
 * This class is responsible for creating objects by converting between domain object classes and entity classes.
 *
 */
@ApplicationScoped
public class PersistenceObjectFactory {

    public SeatDO createSeatDO(Seat seat) {
	if (seat == null) {
	    return null;
	}
	final SeatDO seatDO = new SeatDO(seat.getSeatNumber());
	return seatDO;
    }

    public List<SeatDO> createSeatDOList(Collection<Seat> seats) {
	final List<SeatDO> result = new LinkedList<>();
	for (final Seat seat : seats) {
	    if (seat != null) {
		result.add(createSeatDO(seat));
	    }
	}
	return result;
    }

}
