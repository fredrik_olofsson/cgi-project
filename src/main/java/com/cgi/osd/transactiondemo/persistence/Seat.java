package com.cgi.osd.transactiondemo.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistence class for the seat database table.
 *
 */
@Entity
@Table(name = "seat")
@NamedQueries({ @NamedQuery(name = "Seat.findAll", query = "SELECT s FROM Seat s order by s.seatNumber") })
public class Seat extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "seat_number", nullable = false, unique = true)
    private int seatNumber;

    public Seat() {
    }

    public Seat(int seatNumber) {
	this.seatNumber = seatNumber;
    }

    public int getSeatNumber() {
	return this.seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
	this.seatNumber = seatNumber;
    }

}