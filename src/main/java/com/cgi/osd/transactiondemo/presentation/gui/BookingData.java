package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for holding booking data that needs to be cached for some reason. Should only be accessed
 * using the BookingController to make sure that only one instance will be connected to session.
 *
 *
 */
@Dependent
public class BookingData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private BookingService bookingService;

    @Inject
    private Logger logger;

    private List<SeatDO> seats;

    public List<SeatDO> getAllSeats() {
	return this.seats;
    }

    public void setSeats(List<SeatDO> seats) {
	this.seats = seats;
    }

    @PostConstruct
    private void init() {
	try {
	    this.seats = this.bookingService.getAllSeats();
	} catch (final PersistenceOperationException e) {
	    this.logger.severe("Failed to fetch all seats: " + e.getMessage());
	}
    }
}
